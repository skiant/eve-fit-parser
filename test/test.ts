import { expect } from "chai";
import { describe } from "mocha";

import {
  compareFits,
  getModulesAndCargoList,
  parseFitName,
  parseFitSlots,
  parseShipType,
  isCargoItem,
  parseCargoItem,
  getItemsQuantity,
} from "../index";
import {
  BAD_MALEDICTION,
  BALTEC_APOC_EFT,
  BALTEC_APOC_EVE,
  BOOSTING_EOS,
  BOOSTING_LEGION,
  BOOSTING_STORK,
  SIEGE_PURIFIER_T2,
  STURM_PURIFIER,
} from "./fixtures";

describe("Parsing ship type / fit name", () => {
  it("should return null if there's no ship type", () => {
    expect(parseShipType("")).to.equal(null);
  });

  it("Should find the shiptype", () => {
    expect(parseShipType(BOOSTING_STORK)).to.equal("Stork");
  });

  it("Should find the fit name", () => {
    expect(parseFitName(BOOSTING_STORK)).to.equal("GSF Standard");
  });
});

xdescribe("Parsing modules", () => {
  describe("Stork fit", () => {
    const parsedStork = parseFitSlots(BOOSTING_STORK);

    it("should find low slots", () => {
      expect(parsedStork.lows).to.have.all.members([
        "Damage Control II",
        "Mark I Compact Power Diagnostic System",
      ]);
    });

    it("should find medium slots", () => {
      expect(parsedStork.mediums).to.have.all.members([
        "Multispectrum Shield Hardener II",
        "5MN Quad LiF Restrained Microwarpdrive",
        "Medium Shield Extender II",
        "Medium Shield Extender II",
        "EM Shield Amplifier II",
        "Small F-S9 Regolith Compact Shield Extender",
      ]);
    });

    it("should find high slots", () => {
      expect(parsedStork.highs).to.have.all.members([
        "Information Command Burst II",
        "Information Command Burst II",
        "Shield Command Burst II",
        "Shield Command Burst II",
        "Defender Launcher I",
      ]);
    });

    it("should find rigs", () => {
      expect(parsedStork.rigs).to.have.all.members([
        "Small Core Defense Field Extender II",
        "Small Command Processor I",
      ]);
    });

    it("should find cargo", () => {
      expect(parsedStork.cargo).to.have.all.members([
        "Active Shielding Charge x2000",
        "Shield Harmonizing Charge x2000",
        "Shield Extension Charge x2000",
        "Defender Missile I x1000",
        "Sensor Optimization Charge x2300",
        "Electronic Superiority Charge x2300",
        "Electronic Hardening Charge x2000",
        "Nanite Repair Paste x100",
        "Information Command Mindlink x1",
        "Eifyr and Co. 'Rogue' Navigation NN-603 x1",
        "1MN Afterburner II x1",
      ]);
    });
  });

  describe("Legion fit", () => {
    const parsedLegion = parseFitSlots(BOOSTING_LEGION);

    it("should find low slots", () => {
      expect(parsedLegion.lows).to.have.all.members([
        "Damage Control II",
        "Centus X-Type Thermal Armor Hardener",
        "Reactor Control Unit II",
        "Syndicate 1600mm Steel Plates",
        "Imperial Navy Multispectrum Energized Membrane",
        "Centus X-Type EM Armor Hardener",
        "Imperial Navy 1600mm Steel Plates",
        "Centus X-Type Kinetic Armor Hardener",
      ]);
    });

    it("should find mediums", () => {
      expect(parsedLegion.mediums).to.have.all.members([
        "Cap Recharger II",
        "10MN Afterburner II",
      ]);
    });

    it("should find highs", () => {
      expect(parsedLegion.highs).to.have.all.members([
        "Armor Command Burst II",
        "Information Command Burst II",
        "Skirmish Command Burst II",
        "Covert Ops Cloaking Device II",
      ]);
    });

    it("should find rigs", () => {
      expect(parsedLegion.rigs).to.have.all.members([
        "Medium Ancillary Current Router II",
        "Medium Trimark Armor Pump II",
        "Medium Command Processor I",
      ]);
    });

    it("should find subsystems", () => {
      expect(parsedLegion.subsystems).to.have.all.members([
        "Legion Core - Augmented Antimatter Reactor",
        "Legion Defensive - Augmented Plating",
        "Legion Propulsion - Intercalated Nanofibers",
        "Legion Offensive - Support Processor",
      ]);
    });

    it("should find drones", () => {
      expect(parsedLegion.drones).to.have.all.members([
        "Warrior II x5",
        "Acolyte II x5",
        "Bouncer II x1",
      ]);
    });

    it("should find cargo", () => {
      expect(parseFitSlots(BOOSTING_LEGION).cargo).to.have.all.members([
        "Armor Energizing Charge x900",
        "Rapid Repair Charge x1200",
        "Armor Reinforcement Charge x900",
        "Sensor Optimization Charge x900",
        "Electronic Superiority Charge x900",
        "Electronic Hardening Charge x900",
        "Evasive Maneuvers Charge x900",
        "Interdiction Maneuvers Charge x1200",
        "Rapid Deployment Charge x900",
        "Nanite Repair Paste x50",
        "Mid-grade Amulet Alpha x1",
        "Mid-grade Amulet Epsilon x1",
        "Inherent Implants 'Squire' Capacitor Management EM-803 x1",
        "Mid-grade Amulet Omega x1",
        "High-grade Amulet Beta x1",
        "High-grade Amulet Delta x1",
        "Armored Command Mindlink x1",
        "High-grade Amulet Gamma x1",
        "Agency 'Overclocker' SB5 Dose II x3",
        "Armor Command Burst II x1",
        "Information Command Burst II x1",
        "Skirmish Command Burst II x1",
        "Legion Propulsion - Interdiction Nullifier x1",
        "50MN Quad LiF Restrained Microwarpdrive x1",
        "Legion Defensive - Covert Reconfiguration x1",
      ]);
    });
  });

  describe("Apoc Fit", () => {
    const parsedApoc = parseFitSlots(BALTEC_APOC_EVE);

    it("should find lows", () => {
      expect(parsedApoc.lows).to.have.all.members([
        "Damage Control II",
        "1600mm Steel Plates II",
        "1600mm Steel Plates II",
        "Reactive Armor Hardener",
        "Kinetic Armor Hardener II",
        "Multispectrum Coating II",
        "Heat Sink II",
      ]);
    });
    it("should find mids", () => {
      expect(parsedApoc.mediums).to.have.all.members([
        "Medium F-RX Compact Capacitor Booster",
        "F-90 Compact Sensor Booster",
        "Tracking Computer II",
        "Tracking Computer II",
      ]);
    });
    it("should find highs", () => {
      expect(parsedApoc.highs).to.have.all.members([
        "Mega Beam Laser II",
        "Mega Beam Laser II",
        "Mega Beam Laser II",
        "Mega Beam Laser II",
        "Mega Beam Laser II",
        "Mega Beam Laser II",
        "Mega Beam Laser II",
        "Mega Beam Laser II",
      ]);
    });
    it("should find rigs", () => {
      expect(parsedApoc.rigs).to.have.all.members([
        "Large Trimark Armor Pump I",
        "Large Trimark Armor Pump I",
        "Large Trimark Armor Pump I",
      ]);
    });
    it("should find drones", () => {
      expect(parsedApoc.drones).to.have.all.members([
        "Warrior II x5",
        "Bouncer II x2",
      ]);
    });
    it("should find cargo", () => {
      expect(parsedApoc.cargo).to.have.all.members([
        "Imperial Navy Multifrequency L x16",
        "Optimal Range Script x3",
        "Nanite Repair Paste x50",
        "Imperial Navy Infrared L x16",
        "Navy Cap Booster 800 x30",
        "Targeting Range Script x1",
        "Scan Resolution Script x1",
        "Imperial Navy Standard L x16",
        "Aurora L x16",
        "Gleam L x8",
        "Tracking Speed Script x3",
        "Standard Drop Booster x2",
        "Standard Frentix Booster x3",
        "500MN Y-T8 Compact Microwarpdrive x1",
        "Multispectrum Energized Membrane II x1",
        "Optical Compact Tracking Computer x1",
        "Explosive Armor Hardener II x1",
        "Large Micro Jump Drive x1",
      ]);
    });
  });

  describe("Purifier fit", () => {
    const parsedFit = parseFitSlots(SIEGE_PURIFIER_T2);

    it("should find lows", () => {
      expect(parsedFit.lows).to.have.all.members([
        "Crosslink Compact Ballistic Control System",
        "Micro Auxiliary Power Core I",
        "Ballistic Control System II",
      ]);
    });
    it("should find mediums", () => {
      expect(parsedFit.mediums).to.have.all.members([
        "Peripheral Compact Target Painter",
        "Medium Azeotropic Restrained Shield Extender",
        "5MN Y-T8 Compact Microwarpdrive",
      ]);
    });
    it("should find highs", () => {
      expect(parsedFit.highs).to.have.all.members([
        "Bomb Launcher I",
        "Torpedo Launcher II",
        "Torpedo Launcher II",
        "Torpedo Launcher II",
        "Covert Ops Cloaking Device II",
      ]);
    });
    it("should find rigs", () => {
      expect(parsedFit.rigs).to.have.all.members([
        "Small Processor Overclocking Unit I",
        "Small Processor Overclocking Unit I",
      ]);
    });
    it("should find cargo", () => {
      expect(parsedFit.cargo).to.have.all.members([
        "Electron Bomb x2",
        "Mjolnir Torpedo x5120",
        "Nanite Repair Paste x20",
      ]);
    });
  });

  describe("Malediction fit", () => {
    const parsedMalediction = parseFitSlots(BAD_MALEDICTION);

    it("should find lows", () => {
      expect(parsedMalediction.lows).to.have.all.members([
        "Nanofiber Internal Structure II",
        "Overdrive Injector System II",
        "Overdrive Injector System II",
      ]);
    });

    it("should find mids", () => {
      expect(parsedMalediction.mediums).to.have.all.members([
        "5MN Microwarpdrive II",
        "Warp Disruptor II",
      ]);
    });

    it("should find highs", () => {
      expect(parsedMalediction.highs).to.have.all.members([
        "'Malkuth' Rocket Launcher I",
        "'Malkuth' Rocket Launcher I",
        "'Malkuth' Rocket Launcher I",
      ]);
    });

    it("should find cargo", () => {
      expect(parsedMalediction.cargo).to.have.all.members([
        "Caldari Navy Mjolnir Rocket x2000",
      ]);
    });
  });
});

describe("Getting the list of items", () => {
  it("should parse the Stork fit", () => {
    expect(getModulesAndCargoList(BOOSTING_STORK)).to.have.all.members([
      "Damage Control II",
      "Mark I Compact Power Diagnostic System",
      "Multispectrum Shield Hardener II",
      "5MN Quad LiF Restrained Microwarpdrive",
      "Medium Shield Extender II",
      "Medium Shield Extender II",
      "EM Shield Amplifier II",
      "Small F-S9 Regolith Compact Shield Extender",
      "Information Command Burst II",
      "Information Command Burst II",
      "Shield Command Burst II",
      "Shield Command Burst II",
      "Defender Launcher I",
      "Small Core Defense Field Extender II",
      "Small Command Processor I",
      "Active Shielding Charge x2000",
      "Shield Harmonizing Charge x2000",
      "Shield Extension Charge x2000",
      "Defender Missile I x1000",
      "Sensor Optimization Charge x2300",
      "Electronic Superiority Charge x2300",
      "Electronic Hardening Charge x2000",
      "Nanite Repair Paste x100",
      "Information Command Mindlink x1",
      "Eifyr and Co. 'Rogue' Navigation NN-603 x1",
      "1MN Afterburner II x1",
    ]);
  });

  it("should parse the Legion fit", () => {
    expect(getModulesAndCargoList(BOOSTING_LEGION)).to.have.all.members([
      "Damage Control II",
      "Centus X-Type Thermal Armor Hardener",
      "Reactor Control Unit II",
      "Syndicate 1600mm Steel Plates",
      "Imperial Navy Multispectrum Energized Membrane",
      "Centus X-Type EM Armor Hardener",
      "Imperial Navy 1600mm Steel Plates",
      "Centus X-Type Kinetic Armor Hardener",
      "Cap Recharger II",
      "10MN Afterburner II",
      "Armor Command Burst II",
      "Information Command Burst II",
      "Skirmish Command Burst II",
      "Covert Ops Cloaking Device II",
      "Medium Ancillary Current Router II",
      "Medium Trimark Armor Pump II",
      "Medium Command Processor I",
      "Legion Core - Augmented Antimatter Reactor",
      "Legion Defensive - Augmented Plating",
      "Legion Propulsion - Intercalated Nanofibers",
      "Legion Offensive - Support Processor",
      "Warrior II x5",
      "Acolyte II x5",
      "Bouncer II x1",
      "Armor Energizing Charge x900",
      "Rapid Repair Charge x1200",
      "Armor Reinforcement Charge x900",
      "Sensor Optimization Charge x900",
      "Electronic Superiority Charge x900",
      "Electronic Hardening Charge x900",
      "Evasive Maneuvers Charge x900",
      "Interdiction Maneuvers Charge x1200",
      "Rapid Deployment Charge x900",
      "Nanite Repair Paste x50",
      "Mid-grade Amulet Alpha x1",
      "Mid-grade Amulet Epsilon x1",
      "Inherent Implants 'Squire' Capacitor Management EM-803 x1",
      "Mid-grade Amulet Omega x1",
      "High-grade Amulet Beta x1",
      "High-grade Amulet Delta x1",
      "Armored Command Mindlink x1",
      "High-grade Amulet Gamma x1",
      "Agency 'Overclocker' SB5 Dose II x3",
      "Armor Command Burst II x1",
      "Information Command Burst II x1",
      "Skirmish Command Burst II x1",
      "Legion Propulsion - Interdiction Nullifier x1",
      "50MN Quad LiF Restrained Microwarpdrive x1",
      "Legion Defensive - Covert Reconfiguration x1",
    ]);
  });
});

describe("Cargo items detection", () => {
  it("should catch cargo items", () => {
    expect(isCargoItem("Multispectrum Shield Hardener II")).to.eq(false);
    expect(isCargoItem("Torpedo Launcher II")).to.eq(false);
    expect(isCargoItem("5MN Y-T8 Compact Microwarpdrive")).to.eq(false);
    expect(isCargoItem("Medium F-RX Compact Capacitor Booster")).to.eq(false);

    expect(isCargoItem("Caldari Navy Mjolnir Torpedo x500")).to.eq(true);
    expect(isCargoItem("500MN Y-T8 Compact Microwarpdrive x1")).to.eq(true);
    expect(isCargoItem("Eifyr and Co. 'Rogue' Navigation NN-603 x1")).to.eq(
      true
    );
  });

  it("should get the right item and quantity", () => {
    expect(parseCargoItem("Caldari Navy Mjolnir Torpedo x500")).to.deep.eq({
      name: "Caldari Navy Mjolnir Torpedo",
      quantity: 500,
    });
    expect(parseCargoItem("500MN Y-T8 Compact Microwarpdrive x1")).to.deep.eq({
      name: "500MN Y-T8 Compact Microwarpdrive",
      quantity: 1,
    });
    expect(
      parseCargoItem("Eifyr and Co. 'Rogue' Navigation NN-603 x1")
    ).to.deep.eq({
      name: "Eifyr and Co. 'Rogue' Navigation NN-603",
      quantity: 1,
    });
  });
});

describe("Fit items parsing", () => {
  it("should properly parse the Stork fit", () => {
    expect(getItemsQuantity(BOOSTING_STORK)).to.deep.eq({
      "Damage Control II": 1,
      "Mark I Compact Power Diagnostic System": 1,
      "Multispectrum Shield Hardener II": 1,
      "5MN Quad LiF Restrained Microwarpdrive": 1,
      "Medium Shield Extender II": 2,
      "EM Shield Amplifier II": 1,
      "Small F-S9 Regolith Compact Shield Extender": 1,
      "Information Command Burst II": 2,
      "Shield Command Burst II": 2,
      "Defender Launcher I": 1,
      "Small Core Defense Field Extender II": 1,
      "Small Command Processor I": 1,
      "Active Shielding Charge": 2000,
      "Shield Harmonizing Charge": 2000,
      "Shield Extension Charge": 2000,
      "Defender Missile I": 1000,
      "Sensor Optimization Charge": 2300,
      "Electronic Superiority Charge": 2300,
      "Electronic Hardening Charge": 2000,
      "Nanite Repair Paste": 100,
      "Information Command Mindlink": 1,
      "Eifyr and Co. 'Rogue' Navigation NN-603": 1,
      "1MN Afterburner II": 1,
    });
  });
});

describe("Fits comparison", () => {
  it("Should throw if we are comparing different ships", () => {
    expect(() => compareFits(BOOSTING_EOS, BOOSTING_LEGION)).to.throw();
  });

  it("Should not throw when comparing fits on the same ship", () => {
    expect(() => compareFits(STURM_PURIFIER, SIEGE_PURIFIER_T2)).not.to.throw();
  });

  it("should return the list of extras/missing when comparing fits", () => {
    expect(compareFits(STURM_PURIFIER, SIEGE_PURIFIER_T2)).to.deep.equal({
      extras: [
        "Crosslink Compact Ballistic Control System 1",
        "Micro Auxiliary Power Core I 1",
        "Peripheral Compact Target Painter 1",
        "Medium Azeotropic Restrained Shield Extender 1",
        "5MN Y-T8 Compact Microwarpdrive 1",
        "Covert Ops Cloaking Device II 1",
        "Small Processor Overclocking Unit I 2",
        "Electron Bomb 2",
        "Mjolnir Torpedo 5120",
      ],
      missing: [
        "Damage Control II 1",
        "Co-Processor II 1",
        "Multispectrum Shield Hardener II 1",
        "1MN Afterburner II 1",
        "Medium Shield Extender II 1",
        "Small Ancillary Current Router II 1",
        "Small Core Defense Field Extender II 1",
        "Caldari Navy Mjolnir Torpedo 500",
        "Mjolnir Rage Torpedo 1000",
        "Shrapnel Bomb 2",
        "Mjolnir Javelin Torpedo 300",
        "Focused Void Bomb 2",
        "Nanite Repair Paste 80",
        "Agency 'Pyrolancea' DB3 Dose I 1",
      ],
    });
  });

  it("should handle EFT fit vs EVE fit", () => {
    expect(compareFits(BALTEC_APOC_EFT, BALTEC_APOC_EVE)).to.deep.equal({
      extras: [],
      missing: [],
    });
  });
});
