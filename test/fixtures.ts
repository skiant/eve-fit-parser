export const SCYTHE = `[Scythe, Scy II]
Damage Control II
Capacitor Power Relay II
Capacitor Power Relay II
Capacitor Power Relay II
Capacitor Power Relay II

Enduring Multispectrum Shield Hardener
Large F-S9 Regolith Compact Shield Extender
50MN Quad LiF Restrained Microwarpdrive
Large F-S9 Regolith Compact Shield Extender
Multispectrum Shield Hardener II

Medium Murky Compact Remote Shield Booster
Small Asymmetric Enduring Remote Shield Booster
Small Asymmetric Enduring Remote Shield Booster

Medium EM Shield Reinforcer I
Medium Core Defense Field Extender I
Medium Core Defense Field Extender I



Light Hull Maintenance Bot I x4
Light Armor Maintenance Bot I x5

Nanite Repair Paste x50

`;

export const BOOSTING_STORK = `[Stork, GSF Standard]
Damage Control II
Mark I Compact Power Diagnostic System

Multispectrum Shield Hardener II
5MN Quad LiF Restrained Microwarpdrive
Medium Shield Extender II
Medium Shield Extender II
EM Shield Amplifier II
Small F-S9 Regolith Compact Shield Extender

Information Command Burst II
Information Command Burst II
Shield Command Burst II
Shield Command Burst II
Defender Launcher I

Small Core Defense Field Extender II
Small Command Processor I




Active Shielding Charge x2000
Shield Harmonizing Charge x2000
Shield Extension Charge x2000
Defender Missile I x1000
Sensor Optimization Charge x2300
Electronic Superiority Charge x2300
Electronic Hardening Charge x2000
Nanite Repair Paste x100
Information Command Mindlink x1
Eifyr and Co. 'Rogue' Navigation NN-603 x1
1MN Afterburner II x1

`;

export const BALTEC_APOC_EVE = `[Apocalypse, Balt I]
1600mm Steel Plates II
1600mm Steel Plates II
Damage Control II
Kinetic Armor Hardener II
Reactive Armor Hardener
Multispectrum Coating II
Heat Sink II

Tracking Computer II
Medium F-RX Compact Capacitor Booster
F-90 Compact Sensor Booster
500MN Y-T8 Compact Microwarpdrive

Mega Beam Laser II
Mega Beam Laser II
Mega Beam Laser II
Mega Beam Laser II
Mega Beam Laser II
Mega Beam Laser II
Mega Beam Laser II
Mega Beam Laser II

Large Trimark Armor Pump I
Large Trimark Armor Pump I
Large Trimark Armor Pump I



Warrior II x5
Bouncer II x2

Imperial Navy Multifrequency L x16
Optimal Range Script x3
Tracking Speed Script x3
Nanite Repair Paste x50
Navy Cap Booster 800 x30
Targeting Range Script x1
Scan Resolution Script x1
Imperial Navy Infrared L x16
Imperial Navy Standard L x16
Aurora L x16
Gleam L x8
Standard Drop Booster x2
Standard Frentix Booster x3
Tracking Computer II x1
Optical Compact Tracking Computer x1
Explosive Armor Hardener II x1
Multispectrum Energized Membrane II x1
Large Micro Jump Drive x1

`;

export const BALTEC_APOC_EFT = `
[Apocalypse, GSF Standard v100.1]

1600mm Steel Plates II
1600mm Steel Plates II
Damage Control II
Kinetic Armor Hardener II
Reactive Armor Hardener
Multispectrum Coating II
Heat Sink II

Tracking Computer II, Optimal Range Script
Medium F-RX Compact Capacitor Booster, Navy Cap Booster 800
F-90 Compact Sensor Booster, Targeting Range Script
500MN Y-T8 Compact Microwarpdrive

Mega Beam Laser II, Aurora L
Mega Beam Laser II, Aurora L
Mega Beam Laser II, Aurora L
Mega Beam Laser II, Aurora L
Mega Beam Laser II, Aurora L
Mega Beam Laser II, Aurora L
Mega Beam Laser II, Aurora L
Mega Beam Laser II, Aurora L

Large Trimark Armor Pump I
Large Trimark Armor Pump I
Large Trimark Armor Pump I


Bouncer II x2
Warrior II x5


Standard Frentix Booster


Aurora L x8
Gleam L x8
Navy Cap Booster 800 x29
Imperial Navy Infrared L x16
Imperial Navy Multifrequency L x16
Imperial Navy Standard L x16
Nanite Repair Paste x50
Scan Resolution Script x1
Optimal Range Script x2
Tracking Speed Script x3
Standard Drop Booster x2
Standard Frentix Booster x2
Explosive Armor Hardener II x1
Multispectrum Energized Membrane II x1
Large Micro Jump Drive x1
Optical Compact Tracking Computer x1
Tracking Computer II x1
`;

export const BOOSTING_LEGION = `[Legion, Legion]
Damage Control II
Centus X-Type Thermal Armor Hardener
Reactor Control Unit II
Syndicate 1600mm Steel Plates
Imperial Navy Multispectrum Energized Membrane
Centus X-Type EM Armor Hardener
Imperial Navy 1600mm Steel Plates
Centus X-Type Kinetic Armor Hardener

Cap Recharger II
10MN Afterburner II

Armor Command Burst II
Information Command Burst II
Skirmish Command Burst II
Covert Ops Cloaking Device II

Medium Ancillary Current Router II
Medium Trimark Armor Pump II
Medium Command Processor I

Legion Core - Augmented Antimatter Reactor
Legion Defensive - Augmented Plating
Legion Propulsion - Intercalated Nanofibers
Legion Offensive - Support Processor


Warrior II x5
Acolyte II x5
Bouncer II x1

Armor Energizing Charge x900
Rapid Repair Charge x1200
Armor Reinforcement Charge x900
Sensor Optimization Charge x900
Electronic Superiority Charge x900
Electronic Hardening Charge x900
Evasive Maneuvers Charge x900
Interdiction Maneuvers Charge x1200
Rapid Deployment Charge x900
Nanite Repair Paste x50
Mid-grade Amulet Alpha x1
Mid-grade Amulet Epsilon x1
Inherent Implants 'Squire' Capacitor Management EM-803 x1
Mid-grade Amulet Omega x1
High-grade Amulet Beta x1
High-grade Amulet Delta x1
Armored Command Mindlink x1
High-grade Amulet Gamma x1
Agency 'Overclocker' SB5 Dose II x3
Armor Command Burst II x1
Information Command Burst II x1
Skirmish Command Burst II x1
Legion Propulsion - Interdiction Nullifier x1
50MN Quad LiF Restrained Microwarpdrive x1
Legion Defensive - Covert Reconfiguration x1

`;

export const BOOSTING_EOS = `[Eos, Eos]
Damage Control II
Coreli A-Type Multispectrum Coating
Reactive Armor Hardener
Corpus X-Type EM Armor Hardener
Corpus X-Type Explosive Armor Hardener
Federation Navy 1600mm Steel Plates
Federation Navy 1600mm Steel Plates

Republic Fleet Target Painter
50MN Quad LiF Restrained Microwarpdrive
Cap Recharger II
Cap Recharger II

Skirmish Command Burst II
Skirmish Command Burst II
Drone Link Augmentor II
Drone Link Augmentor II
Drone Link Augmentor II

Medium Trimark Armor Pump II
Medium Trimark Armor Pump II



Hornet EC-300 x10
Curator II x5
Bouncer II x5

Armor Energizing Charge x600
Rapid Repair Charge x600
Armor Reinforcement Charge x600
Evasive Maneuvers Charge x900
Interdiction Maneuvers Charge x900
Rapid Deployment Charge x600
Nanite Repair Paste x50
Skirmish Command Mindlink x1
Armor Command Burst II x2

`;

export const STURM_PURIFIER = `[Purifier, Purifier]
Damage Control II
Co-Processor II
Ballistic Control System II

Multispectrum Shield Hardener II
1MN Afterburner II
Medium Shield Extender II

Bomb Launcher I
Torpedo Launcher II
Torpedo Launcher II
Torpedo Launcher II

Small Ancillary Current Router II
Small Core Defense Field Extender II




Caldari Navy Mjolnir Torpedo x500
Mjolnir Rage Torpedo x1000
Shrapnel Bomb x2
Mjolnir Javelin Torpedo x300
Focused Void Bomb x2
Nanite Repair Paste x100
Agency 'Pyrolancea' DB3 Dose I x1

`;

export const SIEGE_PURIFIER_T2 = `[Purifier, Purifier]
Crosslink Compact Ballistic Control System
Micro Auxiliary Power Core I
Ballistic Control System II

Peripheral Compact Target Painter
Medium Azeotropic Restrained Shield Extender
5MN Y-T8 Compact Microwarpdrive

Bomb Launcher I
Torpedo Launcher II
Torpedo Launcher II
Torpedo Launcher II
Covert Ops Cloaking Device II

Small Processor Overclocking Unit I
Small Processor Overclocking Unit I




Electron Bomb x2
Mjolnir Torpedo x5120
Nanite Repair Paste x20

`;

export const BAD_MALEDICTION = `[Malediction, *BadFit]
Nanofiber Internal Structure II
Overdrive Injector System II
Overdrive Injector System II

5MN Microwarpdrive II
Warp Disruptor II

'Malkuth' Rocket Launcher I
'Malkuth' Rocket Launcher I
'Malkuth' Rocket Launcher I





Caldari Navy Mjolnir Rocket x2000

`;
